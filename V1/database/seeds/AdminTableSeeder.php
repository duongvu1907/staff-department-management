<?php

use Illuminate\Database\Seeder;
use App\Admin;
class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	try {
    		Admin::create([
    			"user_name"=>"root",
    			"email"=>"root@mail.com",
    			"phone"=>"012345678",
    			"last_name"=>"root",
    			"first_name"=>"root",
    			"address"=>"default",
    			"birth_day"=>date("Y-m-d h:i:s",time()),
    			"gender"=>"male",
    			"genre"=>"root",
    			"password"=>Hash::make("12345678"),
    		]);
    		factory(App\Admin::class,5)->create();
    		
    	} catch (Exception $e) {
    		echo $e->getMessage();
    	}
    }
}
