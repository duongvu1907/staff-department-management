<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;
use App\Department;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    $first_name = $faker->unique()->firstName;
    return [
        'email' => $faker->unique()->safeEmail,
        "user_name"=>strtolower($first_name),
        "first_name"=>$first_name,
        "last_name"=>$faker->lastName,
        "phone"=>$faker->phoneNumber,
        "address"=>$faker->address,
        "birth_day"=>$faker->date($format = 'Y-m-d', $max = 'now'),
        "gender"=>$faker->randomElement(['male', 'female','other']),
        "first_login"=>true,
        "department_id"=>Department::inRandomOrder()->first()->id,
        'password' => Hash::make("123"),
    ];
});
