<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Admin;
use Faker\Generator as Faker;

$factory->define(Admin::class, function (Faker $faker) {
    $first_name = $faker->unique()->firstName;
    return [
        'email' => $faker->unique()->safeEmail,
        "user_name"=>strtolower($first_name),
        "first_name"=>$first_name,
        "last_name"=>$faker->lastName,
        "phone"=>$faker->phoneNumber,
        "address"=>$faker->address,
        "birth_day"=>$faker->date($format = 'Y-m-d', $max = 'now'),
        "gender"=>$faker->randomElement(['male', 'female','other']),
        "first_login"=>true,
        "genre"=>"manager",
        'password' => Hash::make("123"),
    ];
});
