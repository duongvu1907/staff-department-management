<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Department;
use Faker\Generator as Faker;

$factory->define(Department::class, function (Faker $faker) {
    $first_name = $faker->unique()->firstName;
    return [
        "name"=>$first_name,
        "address"=>$faker->address,
        "phone"=>$faker->phoneNumber
    ];
});
