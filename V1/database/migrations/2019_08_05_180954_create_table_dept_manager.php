<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDeptManager extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dept_manager', function (Blueprint $table) {
            $table->bigInteger("department_id")->unsigned();
            $table->bigInteger("admin_id")->unsigned();
            $table->foreign("department_id")->references("id")->on("departments")->onDelete("cascade");
            $table->foreign("admin_id")->references("id")->on("administrators")->onDelete("cascade");
            $table->primary(["admin_id","department_id"]);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dept_manager');
    }
}
