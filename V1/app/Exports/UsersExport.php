<?php

namespace App\Exports;


use App\User;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use App\Exports\UsersSheet;
class UsersExport implements WithMultipleSheets
{
	use Exportable;
    
    protected $departments = [];
    public function __construct($departments)
    {	
    	if (is_array($departments)) {
    		$this->departments = $departments;
    	}else{
    		$this->departments = [$departments];
    	}
    	
    }
    public function sheets(): array
    {
    	$sheets = [];
    	foreach ($this->departments as $row) {
    		array_push($sheets,new UsersSheet($row));
    	}
        return $sheets;
    }
}
