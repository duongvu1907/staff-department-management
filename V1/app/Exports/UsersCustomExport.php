<?php  

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use App\User;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
class UsersCustomExport implements FromCollection, ShouldAutoSize, WithHeadings
{
   	protected $ids = [];
    public function __construct($ids)
    {
        $this->ids = is_array($ids)?$ids:[$ids];
    }
    public function collection()
    {
    	$users = User::find($this->ids);
    	$data = [];
    	foreach ($users as $u) {
    		array_push($data, [
                "id"=>$u->id,
                "email"=>$u->email,
                "user_name"=>$u->user_name,
                "first_name"=>$u->first_name,
                "last_name"=>$u->last_name,
                "gender"=>$u->gender,
                "birth_day"=>$u->birth_day,
                "address"=>$u->address,
                "phone"=>addslashes($u->phone),
                "department"=>$u->department->name,
                "first_login"=>$u->first_login?'inactive':'active',
                "created_at"=>$u->created_at,
                "updated_at"=>$u->updated_at
            ]);
    	}
        return collect($data);
    }
    public function headings(): array
    {
        return [
            '#',
            'Email',
            'Username',
            'First name',
            'Last name',
            "Gender",
            "Birth day",
            'Address',
            'Phone',
            "Department",
            "Status",
            'Created at',
            'Updated at'
        ];
    }
}