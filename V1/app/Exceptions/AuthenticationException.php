<?php

namespace App\Exceptions;

use Exception;
use Log;
use Illuminate\Http\Request;
class AuthenticationException extends Exception
{
    public function report()
    {
    	Log::debug(401);
    }
    public function render(Request $request)
    {
    	return response()->json([
    		"message"=>$this->getMessage()
    	],401);
    }
}
