<?php

namespace App\Exceptions;

use Exception;
use Log;
use Illuminate\Http\Request;
class FirstLoginException extends Exception
{
     public function report()
    {
    	Log::debug(400);
    }
    public function render(Request $request)
    {
    	return response()->json([
    		"message"=>$this->getMessage()
    	],400);
    }
}
