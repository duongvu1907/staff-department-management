<?php

namespace App\Exceptions;

use Exception;
use Log;
use Illuminate\Http\Request;
class NotAuthorisedException extends Exception
{
    public function report()
    {
    	Log::debug(403);
    }
    public function render(Request $request)
    {
    	return response()->json([
    		"message"=>$this->getMessage()
    	],403);
    }
}
