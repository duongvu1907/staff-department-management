<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Department extends Model
{
    use SoftDeletes;
    protected $table = "departments";
    protected $fillable = [
    	"name","address","phone","delete_at"
    ];
    protected $dates = ["delete_at"];
    // protected $hidden = ["pivot"];
    public function managers()
    {
    	return $this->belongsToMany(Admin::class,"dept_manager","department_id","admin_id");
    }
    public function users()
    {
    	return $this->hasMany(User::class);
    }
}
