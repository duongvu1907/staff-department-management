<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Database\Eloquent\SoftDeletes;
class Admin extends Authenticatable implements JWTSubject
{
    use SoftDeletes;
	protected $table = "administrators";
    protected $fillable = [
        'first_name','last_name', 'email','user_name',
        'phone','address','birth_day','gender','first_login','genre',"password","delete_at"
    ];
    protected $hidden = [
        'password',
    ];
    protected $dates = ["delete_at"];
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }
    public function departments()
    {
        return $this->belongsToMany(Department::class,'dept_manager','admin_id','department_id');
    }
}
