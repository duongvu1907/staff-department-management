<?php  
namespace App\Http\Controllers;
use Mail;
use Validator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Exceptions\NotAuthorisedException; 
use Hash;

trait CommonTrait{

	public function Validator_UserName_Email($request,$option,$model)
	{

		$option = strtolower(trim($option));
        switch ($option) {
            case "email":
               if ( filter_var($request->email,FILTER_VALIDATE_EMAIL)) {
                    $ignore = $request->ignore?(int)$request->ignore:0;
                    $user = $model::where("id","<>",$ignore)
                        ->where("email",$request->email)
                        ->count();
                    if ($user==0) {
                        return response()->json([
                            "message"=>"Good",
                            "status"=>200
                        ],200);
                    } 
               }
                return response()->json([
                    "message"=>"exists",
                    "status"=>400
                ]);
                break;
            case "user_name":
                $ignore = $request->ignore?(int)$request->ignore:0;
                $user = $model::where("id","<>",$ignore)
                    ->where("user_name",$request->user_name)
                    ->count();
                if ($user==0) {
                    return response()->json([
                        "message"=>"Good",
                        "status"=>200
                    ],200);
                }
                return response()->json([
                    "message"=>"exists or empty",
                    "status"=>400
                ]);
                break;
            }

            return response()->json([
                "message"=>"option fails",
                "status"=>400
            ]);
	}
	public function Authenticate()
    {
        try {
            return auth()->authenticate()->toArray();
        } catch (Exception $e) {
            throw new NotAuthorisedException('You are not authorised to view this page. Please log in.');
        }
    }
    public function Active($request,$model)
    {
        $validators = Validator::make($request->all(),[
            "password"=>"required|confirmed|min:6|max:50",
            "password_confirmation"=>"required|min:6|max:50"
        ]);
        if ($validators->fails()) {
            return response()->json($validators->errors(),400);
        }
        try {
            $user = $model::findOrFail(auth()->authenticate()->id);
            $user->password = Hash::make($request->password);
            $user->first_login = false;
            $user->save();
            return response()->json([
                "message"=>"Successfull",
                "status"=>(int)$user->first_login
            ]);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                "message"=>$e->getMessage(),
                "status"=>400
            ],400);
        }
    }
    public function SendMail($email,$title,$subject,$content,$template)
    {
        try {
            Mail::send(
                $template,
                ["content"=>$content], 
                function($message) use ($email,$title,$subject) 
                {
                    $message->to($email, $title)->subject($subject);
                }
            );
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
    public function Change_Password($request,$model)
    {
    	$validators = Validator::make($request->all(),[
            "password_current"=>"required",
            "password"=>"required|confirmed|min:6|max:50",
            "password_confirmation"=>"required|min:6|max:50"
        ]);
        if ($validators->fails()) {
            return response()->json($validators->errors(),400);
        }
        $current = $model::findOrFail($this->Authenticate()["id"]);
        if (!Hash::check($request->password_current,$current->password)) {
            return response()->json([
                "password_current"=>["password current is not correct"]
            ],400);
        }
        $current->password = Hash::make($request->password);
        $current->save();
        return response()->json([
            "message"=>"successful"
        ]);
    }
    public function Trash_With($model)
    {
        return $model::onlyTrashed()->get();
    }
}
