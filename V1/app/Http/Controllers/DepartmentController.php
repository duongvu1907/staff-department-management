<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\DepartmentResource;
use App\Http\Requests\DepartmentRequest;
use App\Department;
use App\Rules\PhoneValidation;
use Validator;
class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departments = Department::all();
        return response()->json([
            "data"=>DepartmentResource::collection($departments)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DepartmentRequest $request)
    {
        $request->validated();
        $data = Department::create($request->all());
        return response()->json([
            "data"=>$data
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $department = Department::findOrFail($id);
        $manager = $department->managers->map(function($admin){
            return $admin->only(["id"]);
        });
        return response()->json([
            "department"=>$department,
            "manager"=>$manager
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            if ($request->phone) {
                $validator = Validator::make($request->all(),[
                    "phone"=>["required",new PhoneValidation]
                ]);
                if ($validator->fails()) {
                       return response()->json([
                        "message"=>$validator->errors()
                       ],422);
                   }   
            }
            $user = Department::where("id",$id)->update($request->all());
            return response()->json([
                "message"=>"Updated !",
                "data"=>$request->all(),
                "status"=>200
            ],200);
        } catch (Exception $e) {
            return response()->json([
                "message"=>$e->getMessage(),
                "status"=>400
            ],400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();
        $department = Department::findOrFail($id);
        $department->users()->delete();
        $department->delete();
        \DB::commit();
        return response()->json([
            "message"=>"deleted"
        ]);
    }
    public function validation(Request $request)
    {
        $ignore = $request->ignore?(int)$request->ignore:0;
        $departments = Department::where("id","<>",$ignore)->where("name",$request->name)->count();
        if ($departments==0) {
            return response()->json([
                "message"=>"Good",
                "status"=>200
            ],200);
        }
        return response()->json([
            "message"=>"exists",
            "status"=>400
        ],400);
    }
    public function forceDelete($id)
    {
        Department::withTrashed()->where("id",$id)->forceDelete();
        return response()->json([
            "message"=>"deleted"
        ]);
    }
    public function _restore(Request $request)
    {
         $arr = [];
        if (is_array($request->department_id)) {
            $arr=$request->department_id;
        }else{
            $arr=[$request->department_id]; 
        }
        foreach ($arr as $id) {
            Department::withTrashed()->where("id",$id)->restore();
        }
        return response()->json([
            "message"=>"OK"
        ]);
    
    }
    
}
