<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\AdminResource;
use App\Http\Resources\UserResource;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Exceptions\NotAuthorisedException; 
use Illuminate\Validation\Rule;
use App\Http\Controllers\CommonTrait;
use Validator;
use Hash;
use App\Admin;
use App\User;
use Mail;
class AdminController extends Controller
{
    use CommonTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if ($this->Authenticate()["genre"]==="root") {
            if (request("page")==0) {
                return response()->json([
                    "data"=> AdminResource::collection(Admin::where("user_name","<>","root")->get())
                ]);
            }
            $total = Admin::count();
            $admin = Admin::paginate(5);
            return response()->json([
                "data"=> AdminResource::collection($admin),
                "total"=>ceil($total/5)
            ]);
        }
        
    }
    
    public function manager()
    {
        $admin = Admin::findOrFail($this->Authenticate()["id"]);
        $departments = $admin->departments;
        $data = [];
        foreach ($departments as $row) {
            array_push($data,[
                "department"=>$row->name,
                "department_id"=>$row->id,
                "users"=>$row->users
            ]);
        }
        return response()->json([
            "data"=>$data
        ]);
    }
    public function resets(Request $request)
    {
        $validators = Validator::make($request->all(),[
            "admins"=>"required",
        ]);
        if ($validators->fails()) {
            return response()->json($validators->errors(),400);
        }
        $admins = (array) $request->admins;
        foreach ($admins as $id) {
           $this->restore($id);
        }
        return response()->json([
            "message"=>"sent"
        ]);
    }
    public function restore($id)
    {
        $admin = Admin::findOrFail($id);
        $title = "STAFF & DEPARTMENT MANAGEMENT";
        $subject = "Staff & Department";
        $password = str_random(8);
        if ($this->SendMail($admin->email,$title,$subject,
            [
                "password"=>$password,
                "link"=>"http://localhost:3000/admins/login",
                "user_name"=>$admin->user_name
            ],
            "mails/reset_password"
        )) {
            $admin->password = Hash::make($password);
            $admin->first_login = true;
            $admin->save();
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validators = Validator::make($request->all(),[
            "first_name"=>"required",
            "last_name"=>"required",
            "email"=>"required|email|unique:administrators",
            "gender"=>"required|in:male,female",
            "birth_day"=>"required|date",
            "address"=>"required",
            "phone"=>"required",
            "user_name"=>"required|unique:administrators",
            // "genre"=>"required",
            "department_id"=>"required"
        ]);
        if ($validators->fails()) {
            return response()->json($validators->errors(),400);
        }
        $email = $request->email;
        $title = "STAFF & DEPARTMENT MANAGEMENT";
        $subject = "Staff & Department";
        $password = str_random(8);
        if (
            $this->SendMail(
                $email,
                $title,
                $subject,
                [
                    "password"=>$password,
                    "link"=>"http://localhost:3000/login",
                    "user_name"=>$request->user_name
                ],
                "mails/reset_password"
            )
        ) {
            $admin = Admin::create([
                "first_name"=>$request->first_name,
                "last_name"=>$request->last_name,
                "email"=>$request->email,
                "gender"=>$request->gender,
                "address"=>$request->address,
                "phone"=>$request->phone,
                "user_name"=>$request->user_name,
                "password"=>Hash::make($password),
                "birth_day"=>$request->birth_day,
                "genre"=>"manager",
                "first_login"=>true
            ]);
            $admin->departments()->attach($request->department_id);
            return response()->json([
                "message"=>"Create Successfull",
                "status"=>200,
                "data"=>$admin
            ],200);
        }
        return response()->json([
            "message"=>"send email failed",
            "status"=>400,
            "data"=>$request->all()
        ],400); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $admin = Admin::findOrFail($id);
            $departments = $admin->departments;
            $data = [];
            foreach ($departments as $row) {
                array_push($data,$row->id);
            }
            return response()->json([
                "data"=>[
                    "user"=>$admin,
                    "departments"=>$data,
                ],
                "status"=>200
            ],200);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                "message"=>$e->getMessage(),
                "status"=>400
            ],400);
        }
    }
    public function current()
    {
        $id = $this->Authenticate()["id"];
        return $this->show($id);
    }
    public function edit_current(Request $request)
    {
        $id = $this->Authenticate()["id"];
        return $this->update($request, $id);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active(Request $request)
    {
        $validators = Validator::make($request->all(),[
            "password"=>"required|confirmed|min:6|max:50",
            "password_confirmation"=>"required|min:6|max:50"
        ]);
        if ($validators->fails()) {
            return response()->json($validators->errors(),400);
        }
        try {
            $admin = Admin::findOrFail((int)$this->Authenticate()["id"]);
            $admin->password = Hash::make($request->password);
            $admin->first_login = false;
            $admin->save();
            return response()->json([
                "message"=>"Successfull",
                "status"=>(int)$admin->first_login
            ]);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                "message"=>$e->getMessage(),
                "status"=>400
            ],400);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validators = Validator::make($request->all(),[
            "user_name"=>[
                Rule::unique("administrators")->ignore($id)
            ],
            "email"=>[
                Rule::unique("administrators")->ignore($id)
            ]
        ]);
        if ($validators->fails()) {
            return response()->json($validators->errors(),400);
        }
        try {
            $admin = Admin::findOrFail($id);
            $admin->update($request->only([
                "address","first_name","last_name","birth_day",
                "phone","email","gender","user_name"

            ]));
            if ($request->department_id && is_array($request->department_id)) {
                $admin->departments()->sync($request->department_id);
            }
            return response()->json([
                "message"=>"ok",
                "status"=>200
            ],200);
        } catch (Exception $e) {
            return response()->json([
                "message"=>$e->getMessage(),
                "status"=>400
            ],400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Admin::where("user_name","<>","root")->where("id",$id)->first()->delete();
            return response()->json([
                "message"=>"Successfull",
                "status"=>200
            ],200);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                "message"=>$e->getMessage(),
                "status"=>400
            ],400);
        }
    }
    public function validation(Request $request,$option)
    {
        return $this->Validator_UserName_Email($request,$option,app("App\Admin"));    
    }
   
    public function changePassword(Request $request)
    {
        return $this->Change_Password($request,app("App\Admin"));
    }
    public function trash()
    {
        // return response()->json([

        // ],422);
        return response()->json([
            "users"=>$this->Trash_With(app("App\User")),
            "admins"=>$this->Trash_With(app("App\Admin")),
            "departments"=>$this->Trash_With(app("App\Department")),
        ]);
    }
    public function forceDelete($id)
    {
        Admin::withTrashed()->where("id",$id)->forceDelete();
        return response()->json([
            "message"=>"deleted"
        ]);
    }
    public function _restore(Request $request)
    {
         $arr = [];
        if (is_array($request->admin_id)) {
            $arr=$request->admin_id;
        }else{
            $arr=[$request->admin_id]; 
        }
        foreach ($arr as $id) {
            Admin::withTrashed()->where("id",$id)->restore();
        }
        return response()->json([
            "message"=>"OK"
        ]);
    }
}
