<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\UserResource;
use App\Exports\UsersExport;
use App\Exports\UsersCustomExport;
use App\Http\Controllers\CommonTrait;
use App\Rules\PhoneValidation;
use Illuminate\Validation\Rule;
use App\User;
use App\Admin;
use App\Department;
use Validator;
use Mail;
use Hash;
use Excel;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    use CommonTrait;

    public function index()
    {
        $total = User::count();
        $user = User::paginate(10);
        return response()->json([
            "data"=> UserResource::collection($user),
            "total"=>ceil($total/10)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validators = Validator::make($request->all(),[
            "first_name"=>"required",
            "last_name"=>"required",
            "email"=>"required|email|unique:users",
            "gender"=>"required|in:male,female",
            "birth_day"=>"required|date",
            "address"=>"required",
            "phone"=>["required",new PhoneValidation],
            "user_name"=>"required|unique:users",
            "department_id"=>"required"
        ]);
        if ($validators->fails()) {
            return response()->json($validators->errors(),400);
        }
        $email = $request->email;
        $title = "STAFF & DEPARTMENT MANAGEMENT";
        $subject = "Staff & Department";
        $password = str_random(8);
        if (
            $this->sendMail(
                $email,
                $title,
                $subject,
                [
                    "password"=>$password,
                    "link"=>"http://localhost:3000/login",
                    "user_name"=>$request->user_name
                ],
                "mails/reset_password"
            )
        ) {
            $user = User::create([
                "first_name"=>$request->first_name,
                "last_name"=>$request->last_name,
                "email"=>$request->email,
                "gender"=>$request->gender,
                "address"=>$request->address,
                "phone"=>$request->phone,
                "user_name"=>$request->user_name,
                "password"=>Hash::make($password),
                "birth_day"=>$request->birth_day,
                "department_id"=>$request->department_id,
                "first_login"=>true
            ]);
            return response()->json([
                "message"=>"Create Successfull",
                "status"=>200,
                "data"=>$user
            ],200);
        }
        return response()->json([
            "message"=>"send email failed",
            "status"=>400,
            "data"=>$request->all()
        ],400); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        return response()->json([
            "data"=>$user,
            "department"=>$user->department->name,
            "department_id"=>$user->department->id,
        ]);
    }

    public function export(Request $request)
    {
        $admin = Admin::findOrFail(auth()->authenticate()->id);
        $departments = [];
        if ($request->department_id) {
            array_push($departments, (int) $request->department_id);
        }else{
            foreach ($admin->departments as $row) {
                array_push($departments, $row->id);
            }
        }
        
        return Excel::download(new UsersExport($departments),"list.csv");

    }
    public function export_line(Request $request)
    {
        $user_id = is_array($request->user_id)?$request->user_id:[$request->user_id];
        // return json_encode(User::find($user_id));
        return Excel::download(new UsersCustomExport($user_id),"users.csv");
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $validators = Validator::make($request->all(),[
                "first_name"=>"required",
                "last_name"=>"required",
                "email"=>[
                    "required",
                    Rule::unique("users")->ignore($id)
                ],
                "gender"=>"required|in:male,female",
                "birth_day"=>"required|date",
                "address"=>"required",
                "phone"=>["required",new PhoneValidation],
                "user_name"=>[
                    "required",
                    Rule::unique("users")->ignore($id)
                ],
            ]);
            if ($validators->fails()) {
                return response()->json($validators->errors(),400);
            }
            $user = User::where("id",$id)->update($request->only([
                "address","first_name","last_name","birth_day",
                "phone","email","gender","user_name"]));
            return response()->json([
                "message"=>"Updated !",
                "data"=>$request->all(),
                "status"=>200
            ],200);
        } catch (Exception $e) {
            return response()->json([
                "message"=>$e->getMessage(),
                "status"=>400
            ],400);
        }
    }
    public function active(Request $request)
    {
        $validators = Validator::make($request->all(),[
            "password"=>"required|confirmed|min:6|max:50",
            "password_confirmation"=>"required|min:6|max:50"
        ]);
        if ($validators->fails()) {
            return response()->json($validators->errors(),400);
        }
        try {
            $user = User::findOrFail(auth()->authenticate()->id);
            $user->password = Hash::make($request->password);
            $user->first_login = false;
            $user->save();
            return response()->json([
                "message"=>"Successfull",
                "status"=>(int)$user->first_login
            ]);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                "message"=>$e->getMessage(),
                "status"=>400
            ],400);
        }
    }
    public function resets(Request $request)
    {
        $validators = Validator::make($request->all(),[
            "users"=>"required",
        ]);
        if ($validators->fails()) {
            return response()->json($validators->errors(),400);
        }
        $users = (array) $request->users;
        foreach ($users as $id) {
           $this->restore($id);
        }
        return response()->json([
            "message"=>"sent"
        ]);
    }
    public function restore($id)
    {
        $user = User::findOrFail($id);
        $title = "STAFF & DEPARTMENT MANAGEMENT";
        $subject = "Staff & Department";
        $password = str_random(8);
        if ($this->sendMail($user->email,$title,$subject,
            [
                "password"=>$password,
                "link"=>"http://localhost:3000/users/login",
                "user_name"=>$user->user_name
            ],
            "mails/reset_password"
        )) {
            $user->password = Hash::make($password);
            $user->first_login = true;
            $user->save();
        }
    }
    public function current()
    {
        return $this->show(auth()->authenticate()->id);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::findOrFail($id)->delete();
        return response()->json([
            "message"=>"deleted"
        ]);
    }
    public function validation(Request $request,$option)
    {
            return $this->Validator_UserName_Email($request, $option,app("App\User"));
    }
    
    public function update_current(Request $request)
    {
        return $this->update($request,auth()->authenticate()->id);
    }
    public function changePassword(Request $request)
    {
       return $this->Change_Password($request,app("App\User"));
    }
    public function forceDelete($id)
    {
        User::withTrashed()->where("id",$id)->forceDelete();
        return response()->json([
            "message"=>"deleted"
        ]);
    }
    public function _restore(Request $request)
    {

        $arr = [];
        if (is_array($request->user_id)) {
            $arr=$request->user_id;
        }else{
            $arr=[$request->user_id]; 
        }
        foreach ($arr as $id) {
            \DB::beginTransaction();
            $user = User::withTrashed()->where("id",$id)->first();
            if (!$user->department) {
                Department::withTrashed()->where("id",$user->department_id)->restore();
            }
            $user->restore();
            \DB::commit();
        }
        
        return response()->json([
            "message"=>"OK"
        ]);
    }
}
