<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exceptions\NotAuthorisedException; 
use Tymon\JWTAuth\Facades\JWTAuth;
use Auth;
class AuthController extends Controller
{
    public function user(Request $request)
    {
    	try {

            $token = $request->header("Authorization");
            // $user = JWTAuth::user($token);
            auth()->shouldUse("api_admin");
            // return Auth::guard("api_admin")->authenticate();
    		return auth()->authenticate();
    	} catch (Exception $e) {
    		
    	}
    }
}
