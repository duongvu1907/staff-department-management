<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Requests\LoginRequest;
class UserController extends Controller
{
    public function __construct()
	{
		auth()->shouldUse("api_user");
	}
	public function login(LoginRequest $request)
	{
		$request->validated();
		try {
			$credentials = $request->only('user_name', 'password');

			if(!$token = auth()->attempt([
				'user_name' => $request->input('user_name'), 
				'password' => $request->input('password'),
			])) {

				return response()->json([
					'errors' => [
						'user_name' => ['Your user name and/or password may be incorrect.']
					]
				], 422);
			}
		} catch (JWTException $e) {
			return response()->json(['message' => 'Could not create token!'], 401);
		}

		return $this->respondWithToken($token);
	}

	protected function respondWithToken($token)
	{
		return response()->json([
			'access_token' => $token,
			'token_type' => 'bearer',
			'expires_in' => auth()->factory()->getTTL() * 60,
			'user_id' => auth()->user()->id,
			'user_name' => auth()->user()->user_name,
			'genre'=>"employee",
			"first_login"=>(int)auth()->user()->first_login,

    	]);
	}
}
