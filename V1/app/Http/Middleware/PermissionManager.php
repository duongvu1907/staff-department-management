<?php

namespace App\Http\Middleware;

use Closure;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;
use App\Exceptions\NotAuthorisedException; 
use App\Exceptions\NoPermissionException;
class PermissionManager
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next,string $allowPermission=null)
    {
        if ($allowPermission==null) {
            return $next($request);
        }
        try {
            if ($user = auth()->authenticate()->toArray()) {
                if ($user["genre"]==$allowPermission) {
                    return $next($request);
                }
                throw new NoPermissionException('You do not have permission to view this page.');
            }
        } catch (AuthenticationException $e) {
            throw new NotAuthorisedException('You are not authorised to view this page. Please log in.');
        }
    }
}