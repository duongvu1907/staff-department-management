<?php

namespace App\Http\Middleware;

use Closure;
use App\Exceptions\FirstLoginException;
use Illuminate\Auth\AuthenticationException;
use App\Exceptions\NotAuthorisedException;
class FirstLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $user = auth()->authenticate()->toArray();
            if (!$user["first_login"]) {
                return $next($request);
            }
            throw new FirstLoginException("Login and change passowrd now !important");
        } catch (AuthenticationException $e) {
            throw new NotAuthorisedException('You are not authorised to view this page. Please log in.');
        }
        
    }
}
