<?php

namespace App\Http\Middleware;

use Closure;
use Log;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;
use App\Exceptions\NotAuthorisedException; 
use App\Exceptions\NoPermissionException;
class ManageJWT extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next,string $allowGuards=null)
    {
        if ($allowGuards!=null) {
            auth()->shouldUse($allowGuards);
        }
        try {
          if (auth()->authenticate()) {
            return $next($request);
          }  
        } catch (AuthenticationException  $e) {
            try {
              if (!auth()->check()) {
                 return response()->json([
                  "message"=>"The token may have expired"
                ],401); 
              }
              $this->auth->parseToken()->authenticate();
            } catch (TokenExpiredException $e) {
                try {
                    $newtoken = $this->auth->parseToken()->refresh();
                    $response = $next($request);
                    $response->header('Authorization', 'Bearer ' . $newtoken);
                    return $response;
                } 
                catch (TokenExpiredException $e) {

                  throw new NotAuthorisedException('You are not authorised to view this page. Please log in.'); 
                }
            }
        }
        throw new NotAuthorisedException('You are not authorised to view this page. Please log in.');
    }

}
