<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"=>$this->id,
            "first_name"=>$this->first_name,
            "last_name"=>$this->last_name,
            "user_name"=>$this->user_name,
            "first_login"=>(int) $this->first_login,
            "department"=>$this->department->name,
            "email"=>$this->email,
            "phone"=>$this->phone,
            "address"=>$this->address,
            "gender"=>$this->gender,
            "birth_day"=>$this->birth_day,
            "created_at"=>$this->created_at,
        ];
    }
}
