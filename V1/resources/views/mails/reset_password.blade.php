<style type="text/css">
	a{
		padding: 10px 25px;
		color: #fff;
		background:blue;
		border:none;
		border-radius: 5px; 
	}
</style>
<div>
	<h4>Welcome ! </h4>
	<p>Username : {{$content['user_name']}} </p>
	<p>Password : {{$content['password']}} </p>
	<p>
		<a href="{{$content['link']}}">
			Login now
		</a>
	</p>
</div>