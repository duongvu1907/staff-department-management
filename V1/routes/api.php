<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(["prefix"=>"v1",'middleware' => ['api']], function () {
	Route::group(["namespace"=>"Auth"],function(){
		Route::post('admin-login', 'AdminController@login');
		Route::post('user-login', 'UserController@login');
		Route::post('user', 'AuthController@user');

	});
    // ========== Api for Admin ============
	Route::group(["prefix"=>"admin","middleware"=>["manage_jwt:api_admin"]],function(){
		Route::post("/active","AdminController@active");

		Route::group(["middleware"=>"first_login"],function(){
			Route::group(["middleware"=>"manage_permission:root"],function(){

				Route::apiResource("manager","AdminController");
				Route::apiResource("department","DepartmentController");
				Route::apiResource("users","UserController");
				Route::post("/reset-user","UserController@resets");
				Route::post("/reset-admin","AdminController@resets");
				Route::get("/r/statistics","AdminController@statistics");
				Route::get("/trash","AdminController@trash");
				Route::delete("/trash/user/{id}","UserController@forceDelete");
				Route::delete("/trash/admin/{id}","AdminController@forceDelete");
				Route::delete("/trash/department/{id}","DepartmentController@forceDelete");

				Route::post("/trash/user","UserController@_restore");
				Route::post("/trash/admin","AdminController@_restore");
				Route::post("/trash/department","DepartmentController@_restore");

			});
			Route::group(["middleware"=>"manage_permission:manager"],function(){
				Route::get("/manager-user","AdminController@manager");
				Route::post("/user/export","UserController@export");
				Route::post("/user/export-line","UserController@export_line");

				// Route::get("/u/statistics","AdminController@statistics");
			});
			Route::group(["prefix"=>"validate"],function(){
				Route::post("/user/{option}","UserController@validation");
				Route::post("/admin/{option}","AdminController@validation");
				Route::post("department","DepartmentController@validation");
			});
			Route::get("/current","AdminController@current");
			Route::put("/current","AdminController@edit_current");
			Route::post("/change-password","AdminController@changePassword");
		});

	});
    // ========== End ============
    // ========== Api for User =============
	Route::group(["prefix"=>"user","middleware"=>"manage_jwt:api_user"],function(){
		Route::post("/active","UserController@active");
		Route::group(["middleware"=>"first_login"],function(){
			Route::get("/current","UserController@current");
			Route::put("/current","UserController@update_current");
			Route::post("/validate/{option}","UserController@validation");
			Route::post("/change-password","UserController@changePassword");
		});
	});
});
