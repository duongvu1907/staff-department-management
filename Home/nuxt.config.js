module.exports = {
    mode: 'universal',
    /*
     ** Headers of the page
     */
    head: {
        title: process.env.npm_package_name || '',
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
            { rel: "stylesheet", href: '/style.css' },
            { rel: "stylesheet", href: 'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' },
            { rel: "stylesheet", href: "https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css" },
        ],
        script: [
            { src: "https://code.jquery.com/jquery-3.3.1.slim.min.js" },
            { src: "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" },
            { src: "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" },
            { src: "https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.min.js" },

        ]
    },
    /*
     ** Customize the progress-bar color
     */
    loading: { color: 'blue' },
    /*
     ** Global CSS
     */
    css: ['./node_modules/bootstrap/dist/css/bootstrap.css'],
    /*
     ** Plugins to load before mounting the App
     */
    plugins: [
        // "~/plugins/bootstrap",
        "~/plugins/axios"
    ],
    /*
     ** Nuxt.js dev-modules
     */
    devModules: [
        // Doc: https://github.com/nuxt-community/eslint-module
        // '@nuxtjs/eslint-module'
    ],
    /*
     ** Nuxt.js modules
     */
    modules: [
        // Doc: https://axios.nuxtjs.org/usage
        '@nuxtjs/axios'
    ],
    /*
     ** Axios module configuration
     ** See https://axios.nuxtjs.org/options
     */
    axios: {

    },
    /*
     ** Build configuration
     */
    build: {
        // vendor: ['jquery', 'bootstrap'],
        // plugins: [
        //     new webpack.ProvidePlugin({
        //         $: 'jquery',
        //         jQuery: 'jquery',
        //         'window.jQuery': 'jquery'
        //     })
        // ],
        extend(config, { isDev, isClient }) {

        }
    }
}