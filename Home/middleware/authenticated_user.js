export default function({ store, redirect, route }) {
    if (store.getters["login/getAuthenticated"].access_token == undefined || store.getters["login/getAuthenticated"].access_token == '') {
        return redirect("/users/login")
    }

    const middle = route.meta.map((meta) => {
        if (meta.authenticated2 && typeof meta.authenticated2)
            return meta.authenticated2
        return 0
    })
    if (middle[0].role && typeof middle[0].role) {
        console.log(middle[0])
        if (middle[0].role == "first") {
            if (parseInt(store.getters["login/getAuthenticated"].first_login) === 1) {
                return redirect("/users/active")
            }
        }
        if (middle[0].role == "noFirst") {

            if (parseInt(store.getters["login/getAuthenticated"].first_login) === 0) {
                let url = middle[0].url ? middle[0].url : "/users"
                return redirect(url)
            }
        }
    }
}