export default function({ store, redirect, route }) {
    if (store.getters["login/getAuthenticated"].access_token == undefined || store.getters["login/getAuthenticated"].access_token == '') {
        return redirect("/admins/login")
    }
    if (store.getters["login/getAuthenticated"].genre != "manager" && store.getters["login/getAuthenticated"].genre != "root") {
        return redirect("/Fob")
    }
    const middle = route.meta.map((meta) => {
        if (meta.authenticated && typeof meta.authenticated)
            return meta.authenticated
        return 0
    })
    if (middle[0].role && typeof middle[0].role) {
        console.log(middle[0])
        if (middle[0].role == "first") {
            if (parseInt(store.getters["login/getAuthenticated"].first_login) === 1) {
                console.log(store.getters["login/getAuthenticated"].first_login)
                return redirect("/admins/active")
            }
        }
        if (middle[0].role == "noFirst") {

            if (parseInt(store.getters["login/getAuthenticated"].first_login) === 0) {

                let url = middle[0].url ? middle[0].url : "/"
                console.log(url)
                return redirect(url)
            }
        }
    }
}