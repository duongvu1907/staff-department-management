export default function({ store, redirect, route }) {
    if (store.getters["login/getAuthenticated"].access_token != undefined && store.getters["login/getAuthenticated"].access_token != "") {
        if (store.getters["login/getAuthenticated"].genre == "employee") {
            return redirect("/users")
        }
        return redirect("/admins")
    }
}