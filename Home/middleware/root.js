export default function({ store, redirect }) {
    if (store.getters["login/getAuthenticated"].genre != "root") {
        return redirect("/Fob")
    }
}