const state = () => {
    return {
        users: [],
        admins: [],
        errors: {},
        departments: [],
        users_paginate: 0,
        admins_paginate: 0,
        trash: {
            users_trash: [],
            admins_trash: [],
            departments_trash: [],
        },
        statistics: {}
    }
}
const mutations = {
    SET_USERS(state, users) {
        state.users = users
    },
    SET_ADMINS(state, admins) {
        state.admins = admins
    },
    SET_ERRORS(state, errors) {
        state.errors = errors
    },
    SET_DEPARTMENTS(state, departments) {
        state.departments = departments
    },
    CLEAR_ERRORS(state) {
        state.errors = {}
    },
    SET_USERS_PAGINATE(state, page) {
        state.users_paginate = page
    },
    SET_ADMINS_PAGINATE(state, page) {
        state.admins_paginate = page
    },
    SET_STATISTICS(state, payload) {
        state.statistics = payload
    },
    SET_TRASH(state, payload) {
        state.trash.users_trash = payload.users;
        state.trash.admins_trash = payload.admins;
        state.trash.departments_trash = payload.departments;
    }

}
const actions = {
    async USERS({ commit, dispatch }, page) {
        const auth = this.getters["login/getAuthenticated"]
        return await $_GET({
                url: "api/v1/admin/users?page=" + page,
                token: auth.access_token,
                commit,
                self: this
            })
            .then(response => {
                commit("CLEAR_ERRORS")
                if (response.data && response.data !== undefined) {
                    commit("SET_USERS", response.data.data)
                    commit("SET_USERS_PAGINATE", response.data.total)
                }
            })
            .catch(error => {
                if (error.response.status == 400) {
                    // console.log("400")
                    this.commit("login/UPDATE_STATUS", 1)
                }
                if (error.response.status == 401) {
                    this.$router.push('/admins/login')
                }
                commit("SET_ERRORS", error.response)
            })
    },
    async ADMINS({ commit, dispatch }, page) {
        const auth = this.getters["login/getAuthenticated"]
        return await $_GET({
                url: "api/v1/admin/manager?page=" + page,
                token: auth.access_token,
                commit,
                self: this
            })
            .then(response => {
                commit("CLEAR_ERRORS")
                if (response.data && response.data !== undefined) {
                    commit("SET_ADMINS", response.data.data)
                    commit("SET_ADMINS_PAGINATE", response.data.total)
                }
            })
            .catch(error => {
                if (error.response.status == 400) {
                    // console.log("400")
                    this.commit("login/REMOVE_AUTH_COOKIE")
                    this.$router.push('/admins/login')
                }
                if (error.response.status == 401) {
                    this.$router.push('/admins/login')
                }
                commit("SET_ERRORS", error.response)
            })
    },
    async fetchUsersAndAdmins({ dispatch }, page) {
        const auth = this.getters["login/getAuthenticated"]
        if (auth.genre == "root") {
            await dispatch("USERS", page)
            await dispatch("ADMINS", page)
        }
        if (auth.genre == "manager") {
            await dispatch("fetchUserManagement")
        }

    },
    async fetchUserManagement({ commit, dispatch }) {
        const auth = this.getters["login/getAuthenticated"]
        return await $_GET({
                url: "/api/v1/admin/manager-user/",
                token: auth.access_token,
                commit,
                self: this
            })
            .then(response => {
                if (response.data && response.data != undefined) {
                    commit("SET_USERS", response.data.data)
                    commit("SET_USERS_PAGINATE", response.data.total)
                }
            })
            .catch(error => {
                Status_Code(error.response)
            })
    },
    async DEPARTMENTS({ commit, dispatch }) {
        const auth = this.getters["login/getAuthenticated"]
        await $_GET({
                url: "/api/v1/admin/department",
                token: auth.access_token,
                commit,
                self: this
            })
            .then(response => {
                if (response.data && response.data != undefined) {
                    commit("SET_DEPARTMENTS", response.data.data)
                }
            })
            .catch(error => {

            })
    },
    async TRASH({ commit, dispatch, router }) {
        const auth = this.getters["login/getAuthenticated"]
        await $_GET({
                url: "/api/v1/admin/trash",
                token: auth.access_token,
                commit,
                self: this
            })
            .then(response => {
                if (response.data && response.data != undefined) {
                    commit("SET_TRASH", response.data)
                }
            })
            .catch(error => {
                this.commit("login/REMOVE_AUTH_COOKIE")
                    // router.push("/admins/login")
                console.log(error)
            })

    }
}

function $_GET(payload) {
    return new Promise((resolve, reject) => {
        payload.self.$axios.get(payload.url, {
                headers: {
                    Authorization: "Bearer " + payload.token,
                }
            }).then(response => {
                return resolve(response)
            })
            .catch(error => {
                return reject(error)
            })
    })
}

function Status_Code(err) {
    if (err.status == 401) {
        this.commit("REMOVE_AUTH_COOKIE")
        this.$router.push("/admins/login")
    }
}
const getters = {
    getUsers: state => state.users,
    getAdmins: state => state.admins,
    getUsersPaginate: state => state.users_paginate,
    getAdminsPaginate: state => state.admins_paginate,
    getDepartments: state => state.departments,
    getUsersTrash: state => state.trash.users_trash,
    getAdminsTrash: state => state.trash.admins_trash,
    getDepartmentsTrash: state => state.trash.departments_trash,
}
export default { state, mutations, actions, getters }