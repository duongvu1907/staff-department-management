const state = () => ({
    user: {},
    errors: {}
})
const actions = {
    USER({ commit, dispatch }) {
        const auth = this.getters["login/getAuthenticated"]
        this.$axios.get("api/v1/user/current", {
            headers: {
                Authorization: "Bearer " + auth.access_token
            }
        }).then(response => {
            if (response.data && response.data != undefined) {
                commit("SET_USER", response.data.data)
            }
        }).catch(error => {

        })

    },
    ChangePassword({ commit, dispatch }, payload) {
        const auth = this.getters["login/getAuthenticated"]
        this.$axios.post("api/v1/user/change-password", payload, {
            headers: {
                Authorization: "Bearer " + auth.access_token
            }
        }).then(response => {
            commit("CLEAR_ERRORS")
            if (response.data && response.data != undefined) {
                this.commit("login/REMOVE_AUTH_COOKIE")
                this.$router.go("/users/login")
            }
        }).catch(error => {
            commit("SHOW_ERRORS", error.response)
        })
    }
}
const mutations = {
    SET_USER(state, user) {
        state.user = user
    },
    SHOW_ERRORS(state, errors) {
        state.errors = errors
    },
    CLEAR_ERRORS(state) {
        state.errors = {}
    }
}
const getters = {
    getUser: state => state.user,
    getErrors: state => state.errors,
}
export default { state, actions, mutations, getters }