import Cookies from 'js-cookie';

const strict = false;
const state = () => {
    return {
        user: {
            user_name: "",
            password: ""
        },
        errors: {

        },
        authenticated: {
            access_token: "",
            user_id: "",
            user_name: "",
            genre: "",
            first_login: "",
        }
    }
}
const mutations = {
    UPDATE_AUTH_COOKIE(state, token) {
        state.authenticated.access_token = token;
    },
    SET_AUTH_COOKIE(state, payload) {
        if (payload) {
            state.authenticated.access_token = payload.access_token;
            state.authenticated.user_id = payload.user_id;
            state.authenticated.user_name = payload.user_name;;
            state.authenticated.genre = payload.genre;
            state.authenticated.first_login = payload.first_login;
            Cookies.set('mg_token', payload.access_token)
            Cookies.set('mg_user_id', payload.user_id)
            Cookies.set('mg_user_name', payload.user_name)
            Cookies.set('mg_genre', payload.genre)
            Cookies.set('mg_first_login', payload.first_login)
        }
    },
    SET_AUTH_COOKIE_SSR(state, payload) {
        if (payload) {
            state.authenticated.access_token = payload.details.access_token;
            state.authenticated.user_id = payload.details.user_id;
            state.authenticated.user_name = payload.details.user_name;
            state.authenticated.genre = payload.details.genre;
            state.authenticated.first_login = payload.details.first_login;
            payload.res.cookie('mg_token', payload.details.access_token)
            payload.res.cookie('mg_user_id', payload.details.user_id)
            payload.res.cookie('mg_user_name', payload.details.user_name)
            payload.res.cookie('mg_genre', payload.details.genre)
            payload.res.cookie('mg_first_login', payload.details.first_login)
        }
    },
    REMOVE_AUTH_COOKIE(state) {
        state.authenticated.access_token = null;
        state.authenticated.user_id = null;
        state.authenticated.user_name = null;
        state.authenticated.genre = null;
        state.authenticated.first_login = null;
        Cookies.remove('mg_token');
        Cookies.remove('mg_user_id');
        Cookies.remove('mg_user_name');
        Cookies.remove('mg_genre');
        Cookies.remove('mg_first_login');
    },

    CLEAR_ERRORS(state) {
        state.errors = {};
    },
    SHOW_ERRORS(state, payload) {
        state.errors = payload
    },
    UPDATE_STATUS(state, payload) {
        Cookies.set("mg_first_login", payload)
        state.authenticated.first_login = payload
    }
}
const actions = {
    loginAdmin({ commit, dispatch }, payload) {
        const auth = this.getters['login/getAuthenticated'];
        $_POST({
            url: '/api/v1/admin-login',
            token: auth.access_token,
            type: auth.genre,
            params: payload.user,
            commit,
            self: this
        }).then((response) => {
            commit('CLEAR_ERRORS');
            const authenticated = response.data;
            if (authenticated.access_token === '' || authenticated.access_token === undefined) {
                this.$toast.error('Something went wrong. Please try again.');
            } else {
                commit('SET_AUTH_COOKIE', authenticated);
                if (authenticated.first_login) {
                    this.$router.push('/admins/active');
                } else {
                    this.$router.push('/admins');
                }

            }
        }).catch((error) => {
            if (error.response.status === 422) {
                commit('SHOW_ERRORS', { response: error.response });
            }
        })
    },
    loginUser({ commit, dispatch }, payload) {
        const auth = this.getters["login/getAuthenticated"]
        $_POST({
                url: "/api/v1/user-login",
                token: auth.access_token,
                type: auth.genre,
                params: payload.user,
                commit,
                self: this
            })
            .then(response => {
                commit("CLEAR_ERRORS")
                const authenticated = response.data
                if (authenticated.access_token === '' || authenticated.access_token === undefined) {
                    this.$toast.error("Wrong !")
                } else {
                    commit("SET_AUTH_COOKIE", authenticated)
                    this.$router.push("/users")
                }

            })
            .catch(error => {
                if (error.response.status == 422) {
                    commit("SHOW_ERRORS", {
                        error: error.response
                    })
                }
            })
    },
    activeAuthenticated({ commit, dispatch }, payload) {
        const auth = this.getters["login/getAuthenticated"]
        $_POST({
                url: payload.url,
                token: auth.access_token,
                type: auth.genre,
                params: payload.user,
                commit,
                self: this
            })
            .then(response => {
                commit("CLEAR_ERRORS")
                let status = response.data.first_login
                console.log(response.data.first_login)
                commit("UPDATE_STATUS", status)
                if (auth.genre == "employee") {
                    this.$router.push("/users")
                } else {
                    this.$router.push("/admins")
                }
            })
            .catch(error => {
                if (error.response.status == 400) {
                    commit("SHOW_ERRORS", {
                        error: error.response
                    })
                }
                if (error.response.status == 401) {
                    commit("REMOVE_AUTH_COOKIE")
                    alert("Token is expired")
                    this.$router.push("/users/login")
                }
            })
    },
    resetStatusUsers({ commit, dispatch }, payload) {
        const auth = this.getters["login/getAuthenticated"]
        $_POST({
                url: "api/v1/admin/reset-user",
                token: auth.access_token,
                params: payload,
                commit,
                self: this
            })
            .then(response => {
                alert("Successful !")
                this.$router.go("/admins/user")
            })
            .catch(error => {

            })
    },
    resetStatusAdmins({ commit, dispatch }, payload) {
        const auth = this.getters["login/getAuthenticated"]
        $_POST({
                url: "api/v1/admin/reset-admin",
                token: auth.access_token,
                params: payload,
                commit,
                self: this
            })
            .then(response => {
                if (response.data && response.data != undefined) {
                    alert("Sent")
                    this.$router.go("/admins/user")
                }
            })
            .catch(error => {
                alert(error.response)
            })
    },
    changePassword({ commit, dispatch }, payload) {
        const auth = this.getters["login/getAuthenticated"]
        $_POST({
                url: "api/v1/admin/change-password",
                token: auth.access_token,
                params: payload,
                commit,
                self: this
            })
            .then(response => {
                commit("CLEAR_ERRORS")
                if (response.data && response.data != undefined) {
                    // alert("change")
                    commit("REMOVE_AUTH_COOKIE")
                    this.$router.go("/admins/login")
                }
            })
            .catch(error => {
                commit("SHOW_ERRORS", error.response)
            })
    }
}

function $_POST(payload) {
    return new Promise((resolve, reject) => {
        payload.self.$axios
            .post(payload.url, payload.params, {
                headers: {
                    Authorization: "Bearer " + payload.token,
                }
            })
            .then(response => {
                if (response.headers.authorization) {
                    const token = response.headers.authorization.split(
                        "Bearer "
                    );
                    payload.self.commit("login/UPDATE_AUTH_COOKIE", token[1]);
                }
                return resolve(response);
            })
            .catch(error => {
                return reject(error);
            });
    });
}
const getters = {
    getUser: state => state.user,
    getAuthenticated: state => state.authenticated,
    getErrors: state => state.errors,
}
export default { state, mutations, actions, getters, strict }