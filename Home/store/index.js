const state = () => {
    return {

    }
}
const mutations = {

}
const actions = {
    nuxtServerInit({ commit }, { req, res }) {
        if (req.headers.cookie) {
            var cookieparser = require('cookieparser');
            var parsed = cookieparser.parse(req.headers.cookie);
            var details = {
                access_token: parsed.mg_token,
                user_id: parsed.mg_user_id,
                user_name: parsed.mg_user_name,
                genre: parsed.mg_genre,
                first_login: parsed.mg_first_login
            }
            commit('login/SET_AUTH_COOKIE_SSR', { details, res });

        }
    },
}
const getters = {

}
export default { state, mutations, actions, getters }